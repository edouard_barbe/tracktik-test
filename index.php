<?php

require_once(__DIR__.'/app/boot.php');

$console = new Console(249.99);
$tv1 = new Television(499.59);
$tv2 = new Television(1499.00);
$microwave = new Microwave(199.99);

$console->addExtra(new Controller(29.99));
$console->addExtra(new Controller(29.99));
$console->addExtra(new Controller(19.99, 1));
$console->addExtra(new Controller(19.99, 1));
$tv1->addExtra(new Controller(15.00));
$tv1->addExtra(new Controller(15.00));
$tv2->addExtra(new Controller(18.00));

$order = new ElectronicItems([$console, $tv1, $tv2, $microwave]);

print("ITEMS SORTED BY PRICE : <pre>".print_r($order->getSortedItems(),true)."</pre>");
print("TOTAL PRICE : ".$order->getTotalPrice());
print("<br>");
print("CONSOLE WITH CONTROLLERS PRICE : ". $console->getPriceWithExtras());

