<?php

class ElectronicItems
{



    protected $items = array();


    public function __construct(array $items)
    {

        foreach ($items as $item)
        {
            $this->addItem($item);
        }

    }

    public function getItems()
    {
        return $this->items;
    }

    /**
    * Returns the items sorted by price
    *
    * @return array
    */
    public function getSortedItems()
    {

        $sorted = array();
        foreach ($this->items as $item)
        {
            $sorted[($item->getPriceWithExtras() * 100)] = $item;
        }

        ksort($sorted, SORT_NUMERIC);
        return $sorted;
    }

    /**
     *
     * @param string $type
     * @return array
     */
    public function getItemsByType($type)
    {
        if (in_array($type, ElectronicItem::getTypes()))
        {
            $callback = function($item) use ($type)
            {
                return $item->type == $type;
            };

            $items = array_filter($this->items, $callback);
        }

        return false;
    }

    public function addItem($item)
    {

        $this->items[] = $item;

    }

    public function getTotalPrice()
    {
        $total_price = 0;

        foreach($this->items as $item)
        {
            $total_price += $item->getPriceWithExtras();
        }

        return $total_price;
    }
}
