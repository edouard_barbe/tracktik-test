<?php

class Television extends ElectronicItem
{
    public function __construct($price)
    {
        $this->setType(parent::ELECTRONIC_ITEM_TELEVISION);
        $this->setPrice($price);
    }
}