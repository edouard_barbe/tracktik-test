<?php

class Controller extends ElectronicItem
{
    /**
     * @var int
     */
    private $max_extras = 0;

    public function __construct($price, $wired = false)
    {
        $this->setType(parent::ELECTRONIC_ITEM_CONTROLLER);
        $this->setPrice($price);
        $this->setWired($wired);
    }
}