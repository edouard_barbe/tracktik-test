<?php

class Console extends ElectronicItem
{

    /**
     * @var int
     */
    private $max_extras = 4;

    public function __construct($price)
    {
        $this->setType(parent::ELECTRONIC_ITEM_CONSOLE);
        $this->setPrice($price);
    }
}