<?php

class ElectronicItem
{
    /**
     * @var bool
     */
    public $wired;

    /**
    * @var float
    */
    public $price;

    /**
    * @var int
    */
    private $max_extras;

    /**
    * @var string
    */
    private $type;

    /**
    * @var ElectronicItems
    */
    private $extras;

    const ELECTRONIC_ITEM_TELEVISION = 'television';
    const ELECTRONIC_ITEM_CONSOLE = 'console';
    const ELECTRONIC_ITEM_MICROWAVE = 'microwave';
    const ELECTRONIC_ITEM_CONTROLLER = 'controller';

    private static $types = array(self::ELECTRONIC_ITEM_CONSOLE,
    self::ELECTRONIC_ITEM_MICROWAVE, self::ELECTRONIC_ITEM_TELEVISION, self::ELECTRONIC_ITEM_CONTROLLER);

    function getPrice()
    {
        return $this->price;
    }

    function getType()
    {
        return $this->type;
    }

    static function getTypes()
    {
        return self::types;
    }

    function getWired()
    {
        return $this->wired;
    }

    function setPrice($price)
    {
        $this->price = $price;
    }

    function setType($type)
    {
        $this->type = $type;
    }

    function setWired($wired)
    {
        $this->wired = $wired;
    }

    function getPriceWithExtras()
    {
        $full_price = $this->price;

        if($this->extras instanceof ElectronicItems)
        {
            $full_price += $this->extras->getTotalPrice();
        }
        return $full_price;

    }

    function setMaxExtras($max_extras)
    {
        $this->max_extras = $max_extras;
    }

    /**
     * Returns true if maximum amount of extras is reached
     *
     * @return bool
     */
    function maxExtras()
    {

        if(isset($this->max_extras))
        {
            if(count($this->extras->getItems()) >= $this->max_extras)
            {

                return true;

            }
        }

        return false;
    }

    function addExtra($item)
    {
        if(!$this->extras instanceof ElectronicItems)
        {
            $this->extras = new ElectronicItems([$item]);
        }
        else
        {
            $this->extras->addItem($item);
        }
    }
}
