<?php

class Microwave extends ElectronicItem
{
    public function __construct($price)
    {
        $this->setType(parent::ELECTRONIC_ITEM_MICROWAVE);
        $this->setPrice($price);
    }
}