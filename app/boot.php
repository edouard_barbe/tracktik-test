<?php

function ModelLoader($class_name)
{

    if(file_exists(__DIR__.'/models/'.$class_name . '.php'))
    {
        require_once(__DIR__.'/models/'.$class_name . '.php');
        return;
    }

}

spl_autoload_register('ModelLoader');
